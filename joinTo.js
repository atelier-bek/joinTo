function joinTo(el, color, width){
  el.hover(function(){

    var elHovered = $(this);

    elHovered.css({'z-index': '100'});
    $('body').append('<canvas id="canvas" width="' + $(window).width() + '" height="' + $(window).height() + '"></canvas>');

    $('canvas').css({
      position: 'fixed',
      left: 0,
      top: 0,
    });

    var c           = document.getElementById("canvas");
    var ctx         = c.getContext('2d');

    drawLines(el, elHovered, c, ctx, color, width);


    $('*').on('scroll', function(){
      drawLines(el, elHovered, c, ctx, color, width);
    })

  }, function(){
    $('canvas').remove();
    $(this).css({"z-index": 'initial'})
  })
}

function drawLines(el, elHovered, c, ctx, color, width){

  var elPosX      = [];
  var elPosY      = [];
  var elWidth     = [];
  var elHeight    = [];

  var thisWidth   = elHovered.outerWidth();
  var thisHeight  = elHovered.outerHeight();

  el.each(function(){
    elPosX.push($(this).offset().left);
    elPosY.push($(this).offset().top);
    elWidth.push($(this).outerWidth());
    elHeight.push($(this).outerHeight());
  })

  ctx.clearRect(0, 0, c.width, c.height);

  for (var i = 0; i < elPosX.length; i++) {

    var thisPosX  = elHovered.offset().left;
    var thisPosY  = elHovered.offset().top;
    var endX      = elPosX[i];
    var endY      = elPosY[i];

    if (endX != thisPosX) {

      if (endX < thisPosX) {
        endX = endX + elWidth[i];
      } else {
        thisPosX = thisPosX + thisWidth;
      }

      if (endY < thisPosY) {
        endY = endY + elHeight[i];
      } else {
        thisPosY = thisPosY + thisHeight;
      }

      ctx.beginPath();
      ctx.moveTo(thisPosX, thisPosY);
      ctx.lineTo(endX, endY);
      ctx.closePath();
      ctx.strokeStyle = color;
      ctx.lineWidth = width;
      ctx.stroke();

    }
  }
}
