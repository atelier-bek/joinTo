# joinTo

Connect an html element to others with lines.

## Usage

You will need [Jquery](http://jquery.com/).  
Set a specific class to elements you want to connect and give it a css `position` value.

```javascript
jointTo([elements Class], [stroke color], [stroke width]);
```

## Demo

[Here](http://www.atelier-bek.be/bazar/joinTo/)

## ScreenShots

![image1](screenshots/1.png)
![image1](screenshots/2.png)
![image1](screenshots/3.png)
